# Vélos disponibles à Toulouse
> **_Attention:_**  La mise à jour des informations ne se fait plus, puisque l'exécution des jobs GitlabCI du projet a dépassé le nombre limite gratuit. Le `pipeline schedule` a été désactivé le 18/10/2022

Cliquez sur un marqueur pour disposer du nombre de vélos disponibles sur cette station de vélopartage. Zoomez ou cliquez sur les groupements de stations pour accéder aux marqueurs.
<iframe width=700, height=700 frameBorder=0 src="img/carte.html"></iframe>
