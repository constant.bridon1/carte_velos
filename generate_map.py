import requests
import pandas as pd
import folium
from folium.plugins import MarkerCluster
import time
import os
import logging

availability = requests.get('https://transport.data.gouv.fr/gbfs/toulouse/station_status.json').json()
stations = requests.get('https://transport.data.gouv.fr/gbfs/toulouse/station_information.json').json()

try:
    df_availability = pd.DataFrame(availability['data']['stations'])
    df_stations = pd.DataFrame(stations['data']['stations'])
    df = df_availability.merge(df_stations)

    location = (43.604652,  1.444209)

    os.environ['TZ'] = 'Europe/Amsterdam'
    time.tzset()
    carte = folium.Map(location=location)
    marker_cluster = MarkerCluster().add_to(carte)
    for i in df.index:
        folium.Marker([df.lat.loc[i], df.lon.loc[i]],
                    popup=f"""À l'instant 
                    {time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(df.last_reported.loc[i]))}, 
                    il y avait {df.num_bikes_available.loc[i]} vélos disponibles""").add_to(marker_cluster)

    carte.save('docs/img/carte.html')
except KeyError:
    logging.warning('Map generation failed, data were probably not available')
